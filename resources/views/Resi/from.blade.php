@extends('layouts.index')
@section('title','Moselife Admin - Dashboard')
@section('menu','Generate')
@section('root','Resi')
@section('content')
<div class="row">
    @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif
    <form method="POST" action="/Resi/generate">
        @csrf();
    <div class="col-xl-12 order-xl-1">
      <div class="card">
        <div class="card-header">
          <div class="row align-items-center">
            
            <div class="col-8">
              <h3 class="mb-0">Data Resi </h3>
            </div>
            <div class="col-4 text-right">
              <button class="btn btn-sm btn-primary">Buat Resi</button>
            </div>
          </div>
        </div>
        <div class="card-body">
          
            <div class="pl-lg-4">
              <div class="row">
                <div class="col-lg-6">
                  <div class="form-group">
                    <label class="form-control-label" for="input-username">Order Id</label>
                    <input type="text" id="input-username" class="form-control" name="order_id" value="{{ old('order_id') }}" placeholder="Contoh : #INV-1023302-20210902">
                    @if($errors->any())
                        <small class="text-danger">{{$errors->first('order_id')}}</small>
                    @endif
                  </div>
                </div>
                <div class="col-lg-6">
                  <div class="form-group">
                    <label class="form-control-label" for="input-username">Customer Id</label>
                    <input type="text" id="input-username" class="form-control" name="cust_id" value="{{ old('cust_id') }}" placeholder="Custoemr Id">
                    @if($errors->any())
                        <small class="text-danger">{{$errors->first('cust_id')}}</small>
                    @endif
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-lg-4">
                  <div class="form-group">
                    <label class="form-control-label" for="input-username">Kode Layanan</label>
                    <select name="service_code" id="" class="form-control">
                      <option @if (old('service_code') == "REG") selected @endif value="REG">REG</option>
                      <option @if (old('service_code') == "YES") selected @endif value="YES">YES</option>
                      <option @if (old('service_code') == "OKE") selected @endif value="OKE">OKE</option>
                    </select>
                    @if($errors->any())
                        <small class="text-danger">{{$errors->first('service_code')}}</small>
                    @endif
                  </div>
                </div>
                <div class="col-lg-4">
                  <div class="form-group">
                    <label class="form-control-label" for="input-username">Berat (*kg)</label>
                    <input type="text" id="input-username" class="form-control" name="weight" value="{{ old('weight') }}" placeholder="Berat dalam kg">
                    @if($errors->any())
                        <small class="text-danger">{{$errors->first('weight')}}</small>
                    @endif
                  </div>
                </div>
                <div class="col-lg-4">
                  <div class="form-group">
                    <label class="form-control-label" for="input-username">Jumlah Barang</label>
                    <input type="text" id="input-username" class="form-control" name="QTY" value="{{ old('QTY') }}" placeholder="Jumlah barang">
                    @if($errors->any())
                        <small class="text-danger">{{$errors->first('QTY')}}</small>
                    @endif
                  </div>
                </div>
                <div class="col-lg-12">
                  <div class="form-group">
                    <label class="form-control-label" for="input-username">Deskripsi Barang</label>
                    <input type="text" id="input-username" class="form-control" name="GOODS_DESC" value="{{ old('GOODS_DESC') }}" placeholder="Deskripsi barang">
                    @if($errors->any())
                        <small class="text-danger">{{$errors->first('GOODS_DESC')}}</small>
                    @endif
                  </div>
                </div>
              </div>
            </div>
            <hr class="my-4" />
            <h6 class="heading-small text-muted mb-4">Data Pengirim</h6>
            <div class="pl-lg-4">
              <div class="row">
                <div class="col-lg-12">
                  <div class="form-group">
                    <label class="form-control-label" for="input-username">Vendor</label>
                    <select name="vendor" class="form-control" id="">
                      <option value="">Pilih vendor</option>
                      @if ($vendor)
                      @foreach ($vendor as $k => $v)
                          <option @if(old('vendor') == $v->id) selected @endif value="{{$v->id}}">{{$v->nama}} ({{$v->kota}}, {{$v->kecamatan}})</option>
                      @endforeach
                      @endif
                    </select>
                    @if($errors->any())
                        <small class="text-danger">{{$errors->first('vendor')}}</small>
                    @endif
                  </div>
                </div>
              </div>
            </div>
            <hr class="my-4" />
            <h6 class="heading-small text-muted mb-4">Data Penerima</h6>
            <div class="pl-lg-4">
              <div class="row">
                <div class="col-lg-12">
                  <div class="form-group">
                    <label class="form-control-label" for="input-username">Nama</label>
                    <input type="text" id="input-username" class="form-control" value="{{ old('RECEIVER_NAME') }}" name="RECEIVER_NAME" placeholder="Nama">
                    @if($errors->any())
                        <small class="text-danger">{{$errors->first('RECEIVER_NAME')}}</small>
                    @endif
                  </div>
                </div>
                <div class="col-lg-12">
                  <div class="form-group">
                    <label class="form-control-label" for="input-email">Alamat</label>
                    <input type="text" id="input-email" class="form-control" value="{{ old('RECEIVER_ADDR1') }}" name="RECEIVER_ADDR1" placeholder="Alamat">
                    @if($errors->any())
                        <small class="text-danger">{{$errors->first('RECEIVER_ADDR1')}}</small>
                    @endif
                  </div>
                </div>
                <div class="col-lg-4">
                    <div class="form-group">
                      <label class="form-control-label" for="input-email">Kota</label>
                      <input type="text" id="input-email" class="form-control" value="{{ old('RECEIVER_CITY') }}" name="RECEIVER_CITY" placeholder="Kota">
                      @if($errors->any())
                        <small class="text-danger">{{$errors->first('RECEIVER_CITY')}}</small>
                    @endif
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="form-group">
                      <label class="form-control-label" for="input-first-name">Kecamatan</label>
                      <input type="text" id="input-first-name" class="form-control" value="{{ old('RECEIVER_REGION') }}" name="RECEIVER_REGION" placeholder="Kecamatan">
                      @if($errors->any())
                        <small class="text-danger">{{$errors->first('RECEIVER_REGION')}}</small>
                    @endif
                    </div>
                  </div>
                  <div class="col-lg-4">
                    <div class="form-group">
                      <label class="form-control-label" for="input-last-name">KodePos</label>
                      <input type="number" id="input-last-name" class="form-control" value="{{ old('RECEIVER_ZIP') }}" name="RECEIVER_ZIP" placeholder="Kode Pos">
                      @if($errors->any())
                        <small class="text-danger">{{$errors->first('RECEIVER_ZIP')}}</small>
                    @endif
                    </div>
                  </div>
              </div>
              <div class="row">
                <div class="col-lg-6">
                  <div class="form-group">
                    <label class="form-control-label" for="input-first-name">Negara</label>
                    <input type="text" id="input-first-name" class="form-control" name="RECEIVER_COUNTRY" placeholder="Negara" value="INDONESIA" disabled>
                  </div>
                </div>
                <div class="col-lg-6">
                  <div class="form-group">
                    <label class="form-control-label" for="input-last-name">Telepon</label>
                    <input type="number" id="input-last-name" class="form-control" value="{{ old('RECEIVER_CONTACT') }}" name="RECEIVER_CONTACT" placeholder="Hp">
                    @if($errors->any())
                        <small class="text-danger">{{$errors->first('RECEIVER_CONTACT')}}</small>
                    @endif
                  </div>
                </div>
              </div>
            </div>
            <!-- Address -->
            <hr class="my-4" />
            <h6 class="heading-small text-muted mb-4">Pengiriman</h6>
            <div class="pl-lg-4">
              <div class="row">
                <div class="col-lg-6">
                  <div class="form-group">
                    <label class="form-control-label" for="input-city">Asal</label>
                    <select name="origin" id="" class="form-control theSelect">
                        <option value="" class="form-control">Pilih daerah asal</option>
                        @if (isset($origin->detail))
                        @foreach ($origin->detail as $k => $v)
                        <option value="{{$v->City_Code}}|{{$v->City_Name}}" class="form-control">{{$v->City_Name}}</option>
                        @endforeach
                        @endif
                    </select>
                    @if($errors->any())
                        <small class="text-danger">{{$errors->first('origin')}}</small>
                    @endif
                  </div>
                </div>
                <div class="col-lg-6">
                  <div class="form-group">
                    <label class="form-control-label" for="input-country">Tujuan</label>
                    <select name="destination" id="" class="form-control theSelect">
                        <option value="" class="form-control">Pilih daerah tujuan</option>
                        @if (isset($desti->detail))
                        @foreach ($desti->detail as $k => $v)
                        <option value="{{$v->City_Code}}|{{$v->City_Name}}" class="form-control">{{$v->City_Name}}</option>
                        @endforeach
                        @endif
                    </select>
                    @if($errors->any())
                        <small class="text-danger">{{$errors->first('destination')}}</small>
                    @endif
                  </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group">
                      <label class="form-control-label" for="input-country">Tipe</label>
                      <select name="COD_FLAG" id="" class="form-control">
                          <option value="" class="form-control">Pilih COD/NON COD</option>
                          <option @if(old('amount_cod') == "N") selected @endif value="N" class="form-control">NON COD</option>
                          <option @if(old('amount_cod') == "YES") selected @endif value="YES" class="form-control">COD</option>
                      </select>
                      @if($errors->any())
                        <small class="text-danger">{{$errors->first('COD_FLAG')}}</small>
                      @endif
                    </div>
                </div>
                <div class="col-lg-6">
                  <div class="form-group">
                    <label class="form-control-label" for="input-first-name">Harga barang</label>
                    <input type="text" id="input-first-name" class="form-control" name="amount_cod" placeholder="Harga Barang" value="{{ old('amount_cod') }}">
                    @if($errors->any())
                        <small class="text-danger">{{$errors->first('amount_cod')}}</small>
                      @endif
                  </div>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
  
	<script>
		$(".theSelect").select2();
	</script>
  @endsection