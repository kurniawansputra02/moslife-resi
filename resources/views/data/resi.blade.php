@extends('layouts.index')
@section('title','Moselife Admin - Dashboard')
@section('menu','Data')
@section('root','Resi')
@section('content')
   <!-- Card stats -->
   {{-- <div class="row">
    <div class="col-xl-4 col-md-6">
      <div class="card card-stats">
        <!-- Card body -->
        <div class="card-body">
          <div class="row">
            <div class="col">
              <h5 class="card-title text-uppercase text-muted mb-0">Pengiriman selesai</h5>
              <span class="h2 font-weight-bold mb-0">350,897</span>
            </div>
            <div class="col-auto">
              <div class="icon icon-shape bg-gradient-red text-white rounded-circle shadow">
                <i class="ni ni-active-40"></i>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-xl-4 col-md-6">
      <div class="card card-stats">
        <!-- Card body -->
        <div class="card-body">
          <div class="row">
            <div class="col">
              <h5 class="card-title text-uppercase text-muted mb-0">Total Resi</h5>
              <span class="h2 font-weight-bold mb-0">231</span>
            </div>
            <div class="col-auto">
              <div class="icon icon-shape bg-gradient-orange text-white rounded-circle shadow">
                <i class="ni ni-chart-pie-35"></i>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-xl-4 col-md-6">
      <div class="card card-stats">
        <!-- Card body -->
        <div class="card-body">
          <div class="row">
            <div class="col">
              <h5 class="card-title text-uppercase text-muted mb-0">Estimasi Ongkir</h5>
              <span class="h2 font-weight-bold mb-0">231</span>
            </div>
            <div class="col-auto">
              <div class="icon icon-shape bg-gradient-green text-white rounded-circle shadow">
                <i class="ni ni-money-coins"></i>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div> --}}
  <div class="row">
    <div class="col">
      <div class="card ">
        <div class="card-header bg-transparent border-0">
          <h3 class=" mb-0">Transaksi Baru Hari ini</h3>
        </div>
        <div class="table-responsive">
            <table id="example" class="table table-striped" style="width:100%">
                <thead>
                    <tr>
                        <th>Resi</th>
                        <th>Tanggal Dibuat</th>
                        <th>Order Id</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($data as $k => $v)
                    <tr>
                        <td>{{$v->TICKET}}</td>
                        <td>{{$v->created_at}}</td>
                        <td>{{$v->BOOK_CODE}}</td>
                        <td><a href="Detail-{{$v->TICKET}}">Detail</a></td>
                    </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <th>Resi</th>
                        <th>Tanggal Dibuat</th>
                        <th>Order Id</th>
                        <th></th>
                    </tr>
                </tfoot>
            </table>
        </div>
      </div>
    </div>
  </div>
  <script>
    $(document).ready(function() {
        $('#example').DataTable();
    } )
  </script>
  @endsection