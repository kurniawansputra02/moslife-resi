@extends('layouts.index')
@section('title','Moselife Admin - Dashboard')
@section('menu','Data')
@section('root','Detail')
@section('content')
<style>
    /* .hh-grayBox {
	    background-color: #F8F8F8;
	    margin-bottom: 20px;
	    padding: 35px;
        margin-top: 20px;
    } */
    .pt45{padding-top:15px;}
    .order-tracking{
	    text-align: center;
	    width: 15.33%;
	    position: relative;
	    display: block;
    }
.order-tracking .is-complete{
	display: block;
	position: relative;
	border-radius: 50%;
	height: 25px;
	width: 25px;
	border: 0px solid #AFAFAF;
	background-color: #f7be16;
	margin: 0 auto;
	transition: background 0.25s linear;
	-webkit-transition: background 0.25s linear;
	z-index: 2;
}
.order-tracking .is-complete:after {
	display: block;
	position: absolute;
	content: '';
	height: 14px;
	width: 7px;
	top: -2px;
	bottom: 0;
	left: 2px;
	margin: auto 0;
	border: 0px solid #AFAFAF;
	border-width: 0px 2px 2px 0;
	transform: rotate(45deg);
	opacity: 0;
}
.order-tracking.completed .is-complete{
	border-color: #27aa80;
	border-width: 0px;
	background-color: #27aa80;
}
.order-tracking.completed .is-complete:after {
	border-color: #fff;
	border-width: 0px 3px 3px 0;
	width: 7px;
	left: 11px;
	opacity: 1;
}
.order-tracking p {
	color: #A4A4A4;
	font-size: 14px;
	margin-top: 8px;
	margin-bottom: 0;
	line-height: 20px;
}
.order-tracking p span{font-size: 14px;}
.order-tracking.completed p{color: #000;}
.order-tracking::before {
	content: '';
	display: block;
	height: 3px;
	width: calc(100% - 40px);
	background-color: #f7be16;
	top: 13px;
	position: absolute;
	left: calc(-50% + 20px);
	z-index: 0;
}
.order-tracking:first-child:before{display: none;}
.order-tracking.completed:before{background-color: #27aa80;}

</style>
<div class="row">
    @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif
    <form method="POST" action="/Resi/generate">
        @csrf();
    <div class="col-xl-12 order-xl-1">
      <div class="card">
        <div class="card-header">
          <div class="row align-items-center">
            
            <div class="col-8">
              <h3 class="mb-0">Data Resi <i><a href="#">{{$data->TICKET}}</a></i></h3>
            </div>
            
          </div>
        </div>
        <div class="card-body">
          
            <div class="pl-lg-4">
              <div class="row">
                <div class="col-lg-6">
                  <div class="form-group">
                    <label class="form-control-label" for="input-username">Data Pengirim</label>
                    <table class="">
                        <tr>
                            <td><small>Nama</small></td>
                            <td><small>:</small></td>
                            <td><small>{{$data->SHIPPER_NAME}}</small></td>
                        </tr>
                        <tr>
                            <td><small>Alamat</small></td>
                            <td><small>:</small></td>
                            <td><small>{{$data->SHIPPER_ADDR1}}</small></td>
                        </tr>
                        <tr>
                            <td><small>Kota</small></td>
                            <td><small>:</small></td>
                            <td><small>{{$data->SHIPPER_CITY}}</small></td>
                        </tr>
                        <tr>
                            <td><small>Kodepos</small></td>
                            <td><small>:</small></td>
                            <td><small>{{$data->SHIPPER_ZIP}}</small></td>
                        </tr>
                        <tr>
                            <td><small>Kecamatan</small></td>
                            <td><small>:</small></td>
                            <td><small>{{$data->SHIPPER_REGION}}</small></td>
                        </tr>
                        <tr>
                            <td><small>Phone</small></td>
                            <td><small>:</small></td>
                            <td><small>{{$data->SHIPPER_PHONE}}</small></td>
                        </tr>
                    </table>
                  </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group">
                      <label class="form-control-label" for="input-username">Data Penerima</label>
                      <table class="">
                          <tr>
                              <td><small>Nama</small></td>
                              <td><small>:</small></td>
                              <td><small>{{$data->RECEIVER_NAME}}</small></td>
                          </tr>
                          <tr>
                              <td><small>Alamat</small></td>
                              <td><small>:</small></td>
                              <td><small>{{$data->RECEIVER_ADDR1}}</small></td>
                          </tr>
                          <tr>
                              <td><small>Kota</small></td>
                              <td><small>:</small></td>
                              <td><small>{{$data->RECEIVER_CITY}}</small></td>
                          </tr>
                          <tr>
                              <td><small>Kodepos</small></td>
                              <td><small>:</small></td>
                              <td><small>{{$data->RECEIVER_ZIP}}</small></td>
                          </tr>
                          <tr>
                              <td><small>Kecamatan</small></td>
                              <td><small>:</small></td>
                              <td><small>{{$data->RECEIVER_REGION}}</small></td>
                          </tr>
                          <tr>
                              <td><small>Phone</small></td>
                              <td><small>:</small></td>
                              <td><small>{{$data->RECEIVER_PHONE}}</small></td>
                          </tr>
                      </table>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="form-group">
                      <label class="form-control-label" for="input-username">Rincian</label>
                      <table class="table">
                          <tr>
                              <td><small><b>Berat</b></small></td>
                              <td><small><b>:</b></small></td>
                              <td><small>{{$data->WEIGHT}}</small></td>
                              <td><small><b>Jumlah</b></small></td>
                              <td><small><b>:</b></small></td>
                              <td><small>{{$data->QTY}}</small></td>
                              <td><small><b>Keterangan</b></small></td>
                              <td><small><b>:</b></small></td>
                              <td><small>{{$data->GOODS_DESC}}</small></td>
                          </tr>
                          <tr>
                              <td><small><b>Biaya Ongkir</b></small></td>
                              <td><small><b>:</b></small></td>
                              <td><small>{{"Rp " . number_format($data->DELIVERY_PRICE,2,',','.')}}</small></td>
                              <td><small><b>Order Id</b></small></td>
                              <td><small><b>:</b></small></td>
                              <td><small>{{$data->BOOK_CODE}}</small></td>
                              <td><small><b>Branch</b></small></td>
                              <td><small><b>:</b></small></td>
                              <td><small>{{$data->BRANCH}}</small></td>
                          </tr>
                          
                      </table>
                    </div>
                </div>
              </div>
            </div>
            <hr class="my-4" />
            <h6 class="heading-small text-muted mb-4">Status Pengiriman</h6>
            <div class="pl-lg-4">
              <div class="row">
                <div class="col-lg-12">
                    <div class="container">
                        <div class="row">
                            <div class="col-12 col-md-10 hh-grayBox pt45 pb20">
                                <div class="row justify-content-between">
                                    <div class="order-tracking completed">
                                        <span class="is-complete"></span>
                                        <p>Resi dibuat<br><span>Mon, June 24</span></p>
                                    </div>
                                    <div class="order-tracking completed">
                                        <span class="is-complete"></span>
                                        <p>Kurir sedang menjemput paket<br><span>Tue, June 25</span></p>
                                    </div>
                                    <div class="order-tracking completed">
                                        <span class="is-complete"></span>
                                        <p>Paket masuk kegudang penyimpanan<br><span>Fri, June 28</span></p>
                                    </div>
                                    <div class="order-tracking completed">
                                        <span class="is-complete"></span>
                                        <p>Paket diantar kepenerima<br><span>Fri, June 28</span></p>
                                    </div>
                                    <div class="order-tracking">
                                        <span class="is-complete"></span>
                                        <p>Paket sedang diantar<br><span>Fri, June 28</span></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                      </div>
                </div>
              </div>
            </div>
            <hr class="my-4" />
            <!-- Address -->
          </form>
        </div>
      </div>
    </div>
  </div>
  
	<script>
		$(".theSelect").select2();
	</script>
  @endsection