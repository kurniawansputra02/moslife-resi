@extends('layouts.index')
@section('title','Moselife Admin - Dashboard')
@section('menu','Vendor')
@section('root','Data')
@section('content')
@if (isset($vendor))
<div class="row">
    @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif
    <form method="POST" action="/Vendors/Edit/{{$vendor->id}}">
        @csrf();
    <div class="col">
      <div class="card">
        {{-- <div class="card-header">
          <div class="row align-items-center">
            
            <div class="col-8">
              <h3 class="mb-0">Data Vendor </h3>
            </div>
          </div>
        </div> --}}
        <div class="card-body">
            <h6 class="heading-small text-muted mb-4">Data Vendor</h6>
            <div class="pl-lg-4">
              <div class="row">
                <div class="col-lg-12">
                  <div class="form-group">
                    <label class="form-control-label" for="input-username">Nama</label>
                    <input type="text" id="input-username" class="form-control" value="{{$vendor->nama}}" name="name" placeholder="Nama">
                    @if($errors->any())
                        <small class="text-danger">{{$errors->first('name')}}</small>
                    @endif
                  </div>
                </div>
                <div class="col-lg-12">
                  <div class="form-group">
                    <label class="form-control-label" for="input-email">Alamat</label>
                    <input type="text" id="input-email" class="form-control" value="{{$vendor->alamat}}" name="address" placeholder="Alamat">
                    @if($errors->any())
                        <small class="text-danger">{{$errors->first('address')}}</small>
                    @endif
                  </div>
                </div>
                <div class="col-lg-4">
                    <div class="form-group">
                      <label class="form-control-label" for="input-email">Kota</label>
                      <input type="text" id="input-email" class="form-control" value="{{$vendor->kota}}" name="city" placeholder="Kota">
                      @if($errors->any())
                        <small class="text-danger">{{$errors->first('city')}}</small>
                    @endif
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="form-group">
                      <label class="form-control-label" for="input-first-name">Kecamatan</label>
                      <input type="text" id="input-first-name" class="form-control" value="{{$vendor->kecamatan}}" name="region" placeholder="Kecamatan">
                      @if($errors->any())
                        <small class="text-danger">{{$errors->first('region')}}</small>
                    @endif
                    </div>
                  </div>
                  <div class="col-lg-4">
                    <div class="form-group">
                      <label class="form-control-label" for="input-last-name">KodePos</label>
                      <input type="number" id="input-last-name" value="{{$vendor->kodepos}}" class="form-control" name="zip" placeholder="Kode Pos">
                      @if($errors->any())
                        <small class="text-danger">{{$errors->first('zip')}}</small>
                    @endif
                    </div>
                  </div>
              </div>
              <div class="row">
                <div class="col-lg-6">
                  <div class="form-group">
                    <label class="form-control-label" for="input-first-name">Negara</label>
                    <input type="text" id="input-first-name" class="form-control" name="country" placeholder="Negara" value="INDONESIA" disabled>
                    @if($errors->any())
                        <small class="text-danger">{{$errors->first('country')}}</small>
                    @endif
                  </div>
                </div>
                <div class="col-lg-6">
                  <div class="form-group">
                    <label class="form-control-label" for="input-last-name">Telepon</label>
                    <input type="number" id="input-last-name" class="form-control" value="{{$vendor->hp}}" name="contact" placeholder="Hp">
                    @if($errors->any())
                        <small class="text-danger">{{$errors->first('contact')}}</small>
                    @endif
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-lg-12">
                  <button class="btn btn-warning btn-lg btn-block">Ubah</button>
                </div>
              </div>
            </div>
            <hr class="my-4" />
          </form>
        </div>
      </div>
    </div>
</div>
@else
<div class="row">
    @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif
    <form method="POST" action="/Vendors/Create">
        @csrf();
    <div class="col">
      <div class="card">
        {{-- <div class="card-header">
          <div class="row align-items-center">
            
            <div class="col-8">
              <h3 class="mb-0">Data Vendor </h3>
            </div>
          </div>
        </div> --}}
        <div class="card-body">
            <h6 class="heading-small text-muted mb-4">Data Vendor</h6>
            <div class="pl-lg-4">
              <div class="row">
                <div class="col-lg-12">
                  <div class="form-group">
                    <label class="form-control-label" for="input-username">Nama</label>
                    <input type="text" id="input-username" class="form-control" name="name" placeholder="Nama">
                    @if($errors->any())
                        <small class="text-danger">{{$errors->first('name')}}</small>
                    @endif
                  </div>
                </div>
                <div class="col-lg-12">
                  <div class="form-group">
                    <label class="form-control-label" for="input-email">Alamat</label>
                    <input type="text" id="input-email" class="form-control" name="address" placeholder="Alamat">
                    @if($errors->any())
                        <small class="text-danger">{{$errors->first('address')}}</small>
                    @endif
                  </div>
                </div>
                <div class="col-lg-4">
                    <div class="form-group">
                      <label class="form-control-label" for="input-email">Kota</label>
                      <input type="text" id="input-email" class="form-control" name="city" placeholder="Kota">
                      @if($errors->any())
                        <small class="text-danger">{{$errors->first('city')}}</small>
                    @endif
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="form-group">
                      <label class="form-control-label" for="input-first-name">Kecamatan</label>
                      <input type="text" id="input-first-name" class="form-control" name="region" placeholder="Kecamatan">
                      @if($errors->any())
                        <small class="text-danger">{{$errors->first('region')}}</small>
                    @endif
                    </div>
                  </div>
                  <div class="col-lg-4">
                    <div class="form-group">
                      <label class="form-control-label" for="input-last-name">KodePos</label>
                      <input type="number" id="input-last-name" class="form-control" name="zip" placeholder="Kode Pos">
                      @if($errors->any())
                        <small class="text-danger">{{$errors->first('zip')}}</small>
                    @endif
                    </div>
                  </div>
              </div>
              <div class="row">
                <div class="col-lg-6">
                  <div class="form-group">
                    <label class="form-control-label" for="input-first-name">Negara</label>
                    <input type="text" id="input-first-name" class="form-control" name="country" placeholder="Negara" value="INDONESIA" disabled>
                    @if($errors->any())
                        <small class="text-danger">{{$errors->first('country')}}</small>
                    @endif
                  </div>
                </div>
                <div class="col-lg-6">
                  <div class="form-group">
                    <label class="form-control-label" for="input-last-name">Telepon</label>
                    <input type="number" id="input-last-name" class="form-control" name="contact" placeholder="Hp">
                    @if($errors->any())
                        <small class="text-danger">{{$errors->first('contact')}}</small>
                    @endif
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-lg-12">
                  <button class="btn btn-primary btn-lg btn-block">Tambah</button>
                </div>
              </div>
            </div>
            <hr class="my-4" />
          </form>
        </div>
      </div>
    </div>
</div>
@endif
<div class="row">
    <div class="col-xl-12 order-xl-1">
      <div class="card ">
        <div class="card-header bg-transparent border-0">
          <h3 class=" mb-0">Data Vendor</h3>
        </div>
        <div class="table-responsive">
            <table id="example" class="table table-striped" style="width:100%">
                <thead>
                    <tr>
                        <th>Nama</th>
                        <th>Alamat</th>
                        <th>Kota</th>
                        <th>Kecamatan</th>
                        <th>Kodepos</th>
                        <th>Telepon</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @if (isset($data))
                    @foreach ($data as $k => $v)
                    <tr>
                        <td>{{$v->nama}}</td>
                        <td>{{$v->alamat}}</td>
                        <td>{{$v->kota}}</td>
                        <td>{{$v->kecamatan}}</td>
                        <td>{{$v->kodepos}}</td>
                        <td>{{$v->hp}}</td>
                        <td>
                            <a href="/Vendors/Delete/{{$v->id}}" class="text-danger"><i class="fa fa-trash-alt"></i> Hapus</a>
                            <br>
                            <a href="/Vendors-{{$v->nama}}-{{$v->id}}" class="text-warning"><i class="fas fa-edit"></i> Ubah</a>
                        </td>
                    </tr>
                    @endforeach 
                    @endif
                </tbody>
                <tfoot>
                    <tr>
                        <th>Nama</th>
                        <th>Alamat</th>
                        <th>Kota</th>
                        <th>Kecamatan</th>
                        <th>Kodepos</th>
                        <th>Telepon</th>
                        <th></th>
                    </tr>
                </tfoot>
            </table>
        </div>
      </div>
    </div>
  </div>
  
  
	<script>
		$(".theSelect").select2();
	</script>
  @endsection