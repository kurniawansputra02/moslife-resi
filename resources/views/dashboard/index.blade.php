@extends('layouts.index')
@section('title','Moselife Admin - Dashboard')
@section('menu','Dashboard')
@section('root','Dashboard')
@section('content')
   <!-- Card stats -->
   <div class="row">
    <div class="col-xl-4 col-md-6">
      <div class="card card-stats">
        <!-- Card body -->
        <div class="card-body">
          <div class="row">
            <div class="col">
              <h5 class="card-title text-uppercase text-muted mb-0">Pengiriman selesai</h5>
              <span class="h2 font-weight-bold mb-0">350,897</span>
            </div>
            <div class="col-auto">
              <div class="icon icon-shape bg-gradient-red text-white rounded-circle shadow">
                <i class="ni ni-active-40"></i>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-xl-4 col-md-6">
      <div class="card card-stats">
        <!-- Card body -->
        <div class="card-body">
          <div class="row">
            <div class="col">
              <h5 class="card-title text-uppercase text-muted mb-0">Total Resi {{date('M-Y')}}</h5>
              <span class="h2 font-weight-bold mb-0">{{$resi}}</span>
            </div>
            <div class="col-auto">
              <div class="icon icon-shape bg-gradient-orange text-white rounded-circle shadow">
                <i class="ni ni-chart-pie-35"></i>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-xl-4 col-md-6">
      <div class="card card-stats">
        <!-- Card body -->
        <div class="card-body">
          <div class="row">
            <div class="col">
              <h5 class="card-title text-uppercase text-muted mb-0">Estimasi Ongkir {{date('M-Y')}}</h5>
              <span class="h2 font-weight-bold mb-0">{{$estimasi}}</span>
            </div>
            <div class="col-auto">
              <div class="icon icon-shape bg-gradient-green text-white rounded-circle shadow">
                <i class="ni ni-money-coins"></i>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-xl-6 col-md-6">
      <div class="card card-stats">
        <!-- Card body -->
        <div class="card-body">
          <div class="row">
            <div class="col">
              <h5 class="card-title text-uppercase text-muted mb-0">Estimasi Ongkir Non COD {{date('M-Y')}}</h5>
              <span class="h2 font-weight-bold mb-0">{{$price_non}} <br> <small>{{$resi_non}} Resi</small></span>
            </div>
            <div class="col-auto">
              <div class="icon icon-shape bg-gradient-green text-white rounded-circle shadow">
                <i class="ni ni-money-coins"></i>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-xl-6 col-md-6">
      <div class="card card-stats">
        <!-- Card body -->
        <div class="card-body">
          <div class="row">
            <div class="col">
              <h5 class="card-title text-uppercase text-muted mb-0">Estimasi Ongkir COD {{date('M-Y')}}</h5>
              <span class="h2 font-weight-bold mb-0">{{$price_cod}}  <br><small>{{$resi_cod}} Resi</small></span>
            </div>
            <div class="col-auto">
              <div class="icon icon-shape bg-gradient-green text-white rounded-circle shadow">
                <i class="ni ni-money-coins"></i>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col">
      <div class="card bg-default shadow">
        <div class="card-header bg-transparent border-0">
          <h3 class="text-white mb-0">Transaksi Baru Hari ini</h3>
        </div>
        <div class="table-responsive">
          <table class="table align-items-center table-dark table-flush">
            <thead class="thead-dark">
              <tr>
                <th scope="col" class="sort" data-sort="name">Order Id</th>
                <th scope="col" class="sort" data-sort="budget">Tanggal Transaksi</th>
                <th scope="col" class="sort" data-sort="budget">COD</th>
                <th scope="col" class="sort" data-sort="status">Resi</th>
              </tr>
            </thead>
            <tbody class="list">
              @if ($data);
              @foreach ($data as $k => $v)
              <tr>
                <td>{{$v->BOOK_CODE}}</td>
                <td>{{$v->created_at}}</td>
                <td>{{$v->IS_COD}}</td>
                <td>{{$v->TICKET}}</td>
              </tr>
              @endforeach
              @endif
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
  @endsection