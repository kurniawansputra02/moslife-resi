<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tickets extends Model
{
    use HasFactory;

    protected $fillable = [
        'TICKET',
        'SHIPPER_NAME',
        'SHIPPER_ADDR1',
        'SHIPPER_CITY',
        'SHIPPER_ZIP',
        'SHIPPER_REGION',
        'SHIPPER_COUNTRY',
        'SHIPPER_CONTACT',
        'SHIPPER_PHONE',
        'RECEIVER_NAME',
        'RECEIVER_ADDR1',
        'RECEIVER_CITY',
        'RECEIVER_ZIP',
        'RECEIVER_REGION',
        'RECEIVER_COUNTRY',
        'RECEIVER_CONTACT',
        'RECEIVER_PHONE',
        'ORIGIN_DESC',
        'ORIGIN_CODE',
        'DESTINATION_DESC',
        'DESTINATION_CODE',
        'WEIGHT',
        'QTY',
        'GOODS_DESC',
        'DELIVERY_PRICE',
        'BOOK_CODE',
        'CUST_ID',
        'BRANCH',
        'IS_COD',
    ];
}
