<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use GuzzleHttp\Client;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function origin_desti($term)
    {
        $url = 'https://apiv2.jne.co.id:10205';
        $url = ($term == 'origin') ? $url.'/insert/getorigin': $url.'/insert/getdestination';
        $client = new Client();
        
        try {
            $res = $client->post($url, [
                'form_params' => [
                    'username'=>'MOSLIFE',
                    'api_key'=>'87de6e4c4d692cfa661f911a69f9e9bb'
                ]
            ]);
            $body = $res->getBody()->getContents();
            $body = json_decode($body);
            
            return $body;
        } catch (\Throwable $th) {
           return null;
        }
    }
}
