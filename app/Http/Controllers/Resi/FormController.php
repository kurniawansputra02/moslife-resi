<?php

namespace App\Http\Controllers\Resi;

use App\Http\Controllers\Controller;
use App\Models\Tickets;
use Illuminate\Http\Request;
use App\Models\Vendor;
use Illuminate\Support\Facades\Validator;
use Auth;

class FormController extends Controller
{
    public function index()
    {
        $vendor = Vendor::get();
        return view('Resi.from',[
            'vendor' => $vendor,
            'origin' => $this->origin_desti('origin'),
            'desti' => $this->origin_desti('desti')
        ]);
    }

    public function resi(Request $request)
    {
        $rules =[
            'vendor' => 'required',
            'RECEIVER_NAME'=>'required',
            'RECEIVER_ADDR1'=>'required',
            'RECEIVER_CITY'=>'required',
            'RECEIVER_ZIP'=>'required',
            'RECEIVER_REGION'=>'required',
            'RECEIVER_COUNTRY'=>'Indonesia',
            'RECEIVER_CONTACT'=>'required',
            'origin'=>'required',
            'destination'=>'required',
            'weight'=>'required',
            'service_code'=>'required',
            'QTY'=>'required',
            'GOODS_DESC'=>'required',
            'COD_FLAG'=>'required',
            'order_id'=>'required',
            'cust_id'=>'required',
            'amount_cod'=>'required'
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors())->withInput();
        }
        

        // dd($credential);

        // dd($request->all());

        $vendor = Vendor::find($request->vendor);

        $origin = explode('|',$request->origin);
        $desti = explode('|',$request->destination);
        $cod = 0;
        $ongkir = 0;
        
            $curl = curl_init();

            curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://apiv2.jne.co.id:10205/tracing/api/pricedev',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => 'username=MOSLIFE&api_key=87de6e4c4d692cfa661f911a69f9e9bb&from='.$origin[0].'&thru='.$desti[0].'&weight='.$request->weight,
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/x-www-form-urlencoded'
            ),
            ));

            $response = curl_exec($curl);
            $response = json_decode($response);
            curl_close($curl);
        
            foreach ($response->price as $key => $v) {
                if ($v->service_display == $request->service_code) {
                    if ($request->COD_FLAG=="YES") {
                        $cod = $v->price +$request->amount_cod;
                    }
                    $ongkir = $v->price;
                }
            }
        // dd($cod);
        $credential =
        'username=TESTAPI'.
        '&api_key=25c898a9faea1a100859ecd9ef674548'.
        '&OLSHOP_BRANCH=CGK000'.
        '&OLSHOP_CUST=10950700'.//$request->cust_id.
        '&OLSHOP_ORDERID='.$request->order_id.
        '&OLSHOP_SHIPPER_NAME='.$vendor->nama.
        '&OLSHOP_SHIPPER_ADDR1='.$vendor->alamat.
        '&OLSHOP_SHIPPER_ADDR2='.$vendor->kecamatan.
        '&OLSHOP_SHIPPER_CITY='.$vendor->kota.
        '&OLSHOP_SHIPPER_ZIP='.$vendor->kodepos.
        '&OLSHOP_SHIPPER_PHONE='.$vendor->hp.
        '&OLSHOP_RECEIVER_NAME='.$request->RECEIVER_NAME.
        '&OLSHOP_RECEIVER_ADDR1='.$request->RECEIVER_ADDR1.
        '&OLSHOP_RECEIVER_ADDR2='.$request->RECEIVER_REGION.
        '&OLSHOP_RECEIVER_CITY='.$request->RECEIVER_CITY.
        '&OLSHOP_RECEIVER_ZIP='.$request->RECEIVER_ZIP.
        '&OLSHOP_RECEIVER_PHONE='.$request->RECEIVER_CONTACT.
        '&OLSHOP_QTY='.$request->QTY.
        '&OLSHOP_WEIGHT='.$request->weight.
        '&OLSHOP_GOODSDESC='.$request->GOODS_DESC.
        '&OLSHOP_GOODSVALUE='.$request->amount_cod.
        '&OLSHOP_GOODSTYPE=2'.
        '&OLSHOP_INS_FLAG=N'.
        '&OLSHOP_ORIG=CGK10000'.//$origin[0].
        '&OLSHOP_DEST=BDO10000'.//$desti[0].
        '&OLSHOP_SERVICE='.$request->service_code.
        '&OLSHOP_COD_FLAG='.$request->COD_FLAG.
        '&OLSHOP_COD_AMOUNT='.$cod.'';
        $credential = preg_replace( "/\r|\n/", "", $credential );

        // dd($credential);

        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => 'https://apiv2.jne.co.id:10205/tracing/api/generatecnote',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS => $credential,
        CURLOPT_HTTPHEADER => array(
            'Content-Type: application/x-www-form-urlencoded'
        ),
        ));

        $response = curl_exec($curl);
        $response = json_decode($response);
        // dd($response);
        curl_close($curl);
        $t = 'A0'.date('Ymd').rand(1000000,9999999);
        // dd($request->all());
        if (isset($response->status)) {
            if (!$response->status) {
                return redirect()->back()->withInput()->with('status', $response->error);
            }
        }else{
            if ($response->detail[0]->status == "sukses") {
                $data = Tickets::create([
                    'TICKET' => $response->detail[0]->cnote_no,
                    'SHIPPER_NAME'=>$vendor->nama,
                    'SHIPPER_ADDR1'=>$vendor->alamat,
                    'SHIPPER_CITY'=>$vendor->kota,
                    'SHIPPER_ZIP'=>$vendor->kodepos,
                    'SHIPPER_REGION'=>$vendor->kecamatan,
                    'SHIPPER_COUNTRY'=>'Indonesia',
                    'SHIPPER_CONTACT'=>$vendor->hp,
                    'SHIPPER_PHONE'=>$vendor->hp,
                    'RECEIVER_NAME'=>$request->RECEIVER_NAME,
                    'RECEIVER_ADDR1'=>$request->RECEIVER_ADDR1,
                    'RECEIVER_CITY'=>$request->RECEIVER_CITY,
                    'RECEIVER_ZIP'=>$request->RECEIVER_ZIP,
                    'RECEIVER_REGION'=>$request->RECEIVER_REGION,
                    'RECEIVER_COUNTRY'=>'Indonesia',
                    'RECEIVER_CONTACT'=>$request->RECEIVER_CONTACT,
                    'RECEIVER_PHONE'=>$request->RECEIVER_CONTACT,
                    'ORIGIN_DESC'=>$origin[1],
                    'ORIGIN_CODE'=>$origin[0],
                    'DESTINATION_DESC'=>$desti[1],
                    'DESTINATION_CODE'=>$desti[0],
                    'WEIGHT'=>$request->weight,
                    'QTY'=>$request->QTY,
                    'GOODS_DESC'=>$request->GOODS_DESC,
                    'DELIVERY_PRICE'=>$ongkir,
                    'BOOK_CODE'=>$request->order_id,
                    'CUST_ID'=>$request->cust_id,
                    'BRANCH'=>'CGK000',
                    'IS_COD'=>$request->COD_FLAG,
                ]);
    
                return redirect()->back()->with('status', 'Succsess Generate Ticket '. $response->detail[0]->cnote_no)->withInput();
            }else{
                return redirect()->back()->withInput()->with('status', $response->detail[0]->reason);
            }
        }

    }

    public function data()
    {
        $data = Tickets::get();
        return view('data.resi',[
            'data'=>$data
        ]);
    }
    public function detail($id)
    {
        $data = Tickets::where('TICKET',$id)->get();
    
        return view('data.detail',[
            'data'=>$data[0]
        ]);
    }
}
