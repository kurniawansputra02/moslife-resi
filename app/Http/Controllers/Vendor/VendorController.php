<?php

namespace App\Http\Controllers\Vendor;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Vendor;
use Illuminate\Support\Facades\Validator;
use Auth;

class VendorController extends Controller
{
    public function index()
    {
        $data = Vendor::where('status',1)->get();

        return view('data.vendor',[
            'vendor'=>null,
            'data'=>$data
        ]);
    }

    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'address' => 'required',
            'city' => 'required',
            'region' => 'required',
            'zip' => 'required',
            'contact' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        }

        $save = Vendor::create([
            'nama' => $request->name,
            'alamat' => $request->address,
            'kota' => $request->city,
            'kecamatan' => $request->region,
            'kodepos' => $request->zip,
            'hp' => $request->contact,
            'status' => 1,
            'created_by' => Auth::user()->id,
            'updated_by' => Auth::user()->id,
        ]);

        if ($save) {
            return redirect()->back()->with('status', 'Berhasi menambah vendor');
        }else {
            return redirect()->back()->with('status', 'Gagal menambah vendor');
        }
    }

    public function delete($id)
    {
        $find = Vendor::find($id);

        $update = $find->update([
            'status' => 0
        ]);
        if ($update) {
            return redirect()->back()->with('status', 'Berhasi menghapus vendor');
        }else {
            return redirect()->back()->with('status', 'Gagal menghapus vendor');
        }
    }

    public function edit($slug,$id)
    {
        $data = Vendor::where('status',1)->get();
        $vendor = Vendor::find($id);

        return view('data.vendor',[
            'vendor'=>$vendor,
            'data'=>$data
        ]);
    }

    public function update($id,Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'address' => 'required',
            'city' => 'required',
            'region' => 'required',
            'zip' => 'required',
            'contact' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        }
        $vendor = Vendor::find($id);

        $save = $vendor->update([
            'nama' => $request->name,
            'alamat' => $request->address,
            'kota' => $request->city,
            'kecamatan' => $request->region,
            'kodepos' => $request->zip,
            'hp' => $request->contact,
            'updated_by' => Auth::user()->id,
        ]);
        
        if ($save) {
            return redirect('/Vendors')->with('status', 'Berhasi mengubah vendor');
        }else {
            return redirect()->back()->with('status', 'Gagal mengubah vendor');
        }
    }
}
