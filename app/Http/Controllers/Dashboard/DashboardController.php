<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use App\Models\Tickets;

class DashboardController extends Controller
{
    public function index()
    {
        $data = Tickets::where('created_at' ,'>=', date('Y-m-d'))->get();
        $dev_price = 0;
        $resi = 0;

        $datas = Tickets::where('created_at' ,'>=', date('Y-m-01'))->where('created_at' ,'<=', date('Y-m-d', strtotime('+1 day')))->get();
        foreach ($datas as $k => $v) {
            $dev_price =$dev_price +$v->DELIVERY_PRICE;
            $resi = 1+$resi;
        }

        $price_non = 0;
        $resi_non = 0;
        $non = Tickets::where('IS_COD','=','Y')->orWhere('IS_COD','=',null)->where('created_at' ,'>=', date('Y-m-01'))->where('created_at' ,'<=', date('Y-m-d', strtotime('+1 day')))->get();
        foreach ($non as $k => $v) {
            $price_non =$price_non +$v->DELIVERY_PRICE;
            $resi_non = 1+$resi_non;
        }

        $price_cod = 0;
        $resi_cod = 0;
        $cod = Tickets::where('IS_COD','=','YES')->where('created_at' ,'>=', date('Y-m-01'))->where('created_at' ,'<=', date('Y-m-d', strtotime('+1 day')))->get();
        foreach ($cod as $k => $v) {
            $price_cod =$price_cod +$v->DELIVERY_PRICE;
            $resi_cod = 1+$resi_cod;
        }

        
        return view('dashboard.index',[
            'data' => $data,
            'estimasi' => "Rp " . number_format($dev_price,2,',','.'),
            'resi' => $resi,
            'price_cod' => "Rp " . number_format($price_cod,2,',','.'),
            'resi_cod' => $resi_cod,
            'price_non' => "Rp " . number_format($price_non,2,',','.'),
            'resi_non' => $resi_non,
            
        ]);
    }
}
