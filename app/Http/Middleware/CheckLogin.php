<?php

namespace App\Http\Middleware;

use Closure;

class CheckLogin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $role = null)
    {
        if(auth()->check() == true) {
            // if(auth()->user()->level == $role) {
                return $next($request);
            // }
        }else {
            return back()->withErrors(['wrong'=>"You'r session is expired, pleas login !"]);
        }
        abort(404);
    }
}
