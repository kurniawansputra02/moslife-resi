<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tickets', function (Blueprint $table) {
            $table->id();
            $table->string('TICKET');
            $table->string('SHIPPER_NAME');
            $table->string('SHIPPER_ADDR1');
            $table->string('SHIPPER_CITY');
            $table->string('SHIPPER_ZIP');
            $table->string('SHIPPER_REGION');
            $table->string('SHIPPER_COUNTRY');
            $table->string('SHIPPER_CONTACT');
            $table->string('SHIPPER_PHONE');
            $table->string('RECEIVER_NAME');
            $table->string('RECEIVER_ADDR1');
            $table->string('RECEIVER_CITY');
            $table->string('RECEIVER_ZIP');
            $table->string('RECEIVER_REGION');
            $table->string('RECEIVER_COUNTRY');
            $table->string('RECEIVER_CONTACT');
            $table->string('RECEIVER_PHONE');
            $table->string('ORIGIN_DESC');
            $table->string('ORIGIN_CODE');
            $table->string('DESTINATION_DESC');
            $table->string('DESTINATION_CODE');
            $table->string('WEIGHT');
            $table->string('QTY');
            $table->string('GOODS_DESC');
            $table->string('DELIVERY_PRICE');
            $table->string('BOOK_CODE');
            $table->string('CUST_ID');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tickets');
    }
}
