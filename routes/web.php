<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => 'Dashboard','namespace' => 'Dashboard','middleware' => 'CheckLogin'],function () {
    
    Route::get("/", 'DashboardController@index');

});
Route::group(['prefix' => 'Resi','namespace' => 'Resi','middleware' => 'CheckLogin'],function () {
    Route::get("/", 'FormController@index');
    Route::post("/generate", 'FormController@resi');
    Route::get("/Data", 'FormController@index');
});
Route::group(['namespace' => 'Resi','middleware' => 'CheckLogin'],function () {
    Route::get("/Data", 'FormController@data');
    Route::get("/Detail-{id}", 'FormController@detail');
});
Route::group(['namespace' => 'Vendor','middleware' => 'CheckLogin'],function () {
    Route::get("/Vendors", 'VendorController@index');
    Route::get("/Vendors-{slug}-{id}", 'VendorController@edit');
    Route::post("/Vendors/Create", 'VendorController@create');
    Route::post("/Vendors/Edit/{id}", 'VendorController@update');
    Route::get("/Vendors/Delete/{id}", 'VendorController@delete');
});
Route::get('/', function (){
    return view('login');
});
Route::post('/login','Login\LoginController@login');

Route::get('/logout', function () {
    Auth::logout();
    return redirect('/');
});
